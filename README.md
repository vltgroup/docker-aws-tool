# Docker tools for AWS

Helper container to build, push and deploy things on docker running on AWS.
This image is based on latest alpine image to have minimal possible footprint size.
In addition it has AWS CLI and SSH installed as well as the docker client CLI.

## Commands

### Build docker container, tag it and push into ECS registry

Will compile, tag and push the image into ECS registry. Docker socket shall be mounted as the images doesn't provide docker host.

`docker-aws-tool build repo/image tag1 tag2 ...`

You can assign labels provided LABELS environment variable.

#### Example:

```bash
docker run --rm \
  -v "/var/run/docker.sock:/var/run/docker.sock" \
  -v "$(pwd):/home" \
  -e "AWS_ACCESS_KEY_ID=XXXYYYZZZ" \
  -e "AWS_SECRET_ACCESS_KEY=AAABBBCCCDDDEEEFFFGGGHHH" \
  -e "LABELS=version=1.23 commit=ef624" \
  vltgroup/docker-aws-tool build 1112223334445.dkr.ecr.eu-west-1.amazonaws.com/namespace/image tag1 tag2 tag3
```

alternatively credentials file can be used:

```bash
docker run --rm \
  -v "/var/run/docker.sock:/var/run/docker.sock" \
  -v "$(pwd):/home" \
  -v "/home/user/.aws/credentials:/root/.aws/credentials:ro" \
  -e "LABELS=version=1.23 commit=ef624" \
  vltgroup/docker-aws-tool build 1112223334445.dkr.ecr.eu-west-1.amazonaws.com/namespace/image tag1 tag2 tag3
```

### Deploy docker stack on AWS hosted swarm using multiple files

The command can join multiple YAML files and deploy the stack.
Syntax

`docker run --rm vltgroup/docker-aws-tool deploy-files <stackname> user@manager.host compose1.yml compose2.yml ... `

### Switch image tag for the running service

Using this command it's easy to switch between image tags for the running service.

#### Example:

```bash
docker run --rm \
  -v "/home/user/.ssl/remote.pem:/root/.ssh/id_rsa:ro" \
  -v "/home/user/.aws/credentials:/root/.aws/credentials:ro" \
  vltgroup/docker-aws-tool switch-image docker@ec2-host.aws.amazon.com running_service_name newtag
```

This will update image for the service running_service_name.


## Environment variables

The following variables are used for authenticate in ECS registry

* AWS_ACCESS_KEY_ID

* AWS_SECRET_ACCESS_KEY

* AWS_DEFAULT_REGION

* LABELS (optional)
Can be used with build command to provide extra labels for the image.
